package ${core-plugin}.impl;

import ${core-plugin}.GreetingService;

public class DefaultGreetingService implements GreetingService {

    @Override
    public String greet() {
        return "Hello World";
    }

}
