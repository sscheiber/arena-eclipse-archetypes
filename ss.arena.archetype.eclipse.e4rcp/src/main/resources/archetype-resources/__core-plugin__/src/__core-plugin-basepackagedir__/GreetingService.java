package ${core-plugin};

public interface GreetingService {
    public String greet();
}