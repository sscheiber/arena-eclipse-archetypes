package ${core-plugin};

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ${core-plugin}.impl.DefaultGreetingService;

public class GreetingServiceTest {
    
    private GreetingService greetingService = new DefaultGreetingService();
    
    @Test
    public void testGreet() {
        assertEquals("Hello World", greetingService.greet());
    }
}


